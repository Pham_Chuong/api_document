# Manifest
## Create manifest

> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://services.boxme.vn/api/manifests")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request["cache-control"] = 'no-cache'
request["postman-token"] = 'efeb234f-600b-e7ce-ea49-ab8b6ac3a57c'
request.body = "{ \"token\":\"29cf9d7c9c6d1ecef3812034c75f34ec\",  \"reference_code\":\"MF-TEST\",    \"inventory_id\":186019,\"entry_point\":\"VTPHN01\", \"overpacks\":[ {\"description\" : \"Test manifes boxme\",\"volume\" : \"10x10x10\",\"weight\" : 1100,\"color\" : \"red\",\"total\" : 100,\"overpack_partner_code\" : \"MFXXXXXX\",\"manifest_orders\":[ {\"tracking_code\" : \"SC5388176946\",\"quantity_tracking\" : 50,\"volume_tracking\" : \"10x10x10\",\"color_tracking\" : \"Red\",\"material_tracking\" : \"Plastic\",\"qc_tracking\" : \"Test\",\"tag_warehouse_code\" : \"MFXXXXXX\",\"partner_code\" : \"MFXXXXXX\",\"weight_tracking\" : 100,\"images\":[{\"tracking_code\" : \"SC5388176946\",\"url\" : \"http://placehold.it/170x100&text=one\"},{\"tracking_code\" : \"SC5388176946\",\"url\" : \"http://placehold.it/170x100&text=one\"}]} ]} ]}"

response = http.request(request)
puts response.read_body

```

```python
import requests

url = "http://services.boxme.vn/api/manifests"

payload = "{    \"token\":\"29cf9d7c9c6d1ecef3812034c75f34ec\", \"reference_code\":\"MF-TEST\",  \"inventory_id\":186019, \"entry_point\":\"VTPHN01\",\"overpacks\":[  { \"description\" : \"Test manifes boxme\",\"volume\" : \"10x10x10\",  \"weight\" : 1100, \"color\" : \"red\", \"total\" : 100,\"overpack_partner_code\" : \"MFXXXXXX\",\"manifest_orders\":[ {  \"tracking_code\" : \"SC5388176946\",\"quantity_tracking\" : 50,\"volume_tracking\" : \"10x10x10\",\"color_tracking\" : \"Red\",\"material_tracking\" : \"Plastic\",\"qc_tracking\" : \"Test\",\"tag_warehouse_code\" : \"MFXXXXXX\",\"partner_code\" : \"MFXXXXXX\",\"weight_tracking\" : 100,\"images\":[{\"tracking_code\" : \"SC5388176946\",\"url\" : \"http://placehold.it/170x100&text=one\"},{\"tracking_code\" : \"SC5388176946\",\"url\" : \"http://placehold.it/170x100&text=one\"}] }]}]}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "2c7edb7b-ecfa-1824-5722-1752155bfdf1"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
```

```shell
curl --request POST \
  --url http://services.boxme.vn/api/manifests \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/json' \
  --header 'postman-token: f6f86e0a-d1d2-ce55-db9d-ca5c77bf65c1' \
  --data '{"token":"29cf9d7c9c6d1ecef3812034c75f34ec",  "reference_code":"MF-TEST","inventory_id":186019, "entry_point":"VTPHN01",    "overpacks":[{"description" : "Test manifes boxme","volume" : "10x10x10",       "weight" : 1100, "color" : "red","total" : 100, "overpack_partner_code" : "MFXXXXXX","manifest_orders":[ {"tracking_code" : "SC5388176946","quantity_tracking" : 50,	"volume_tracking" : "10x10x10",			"color_tracking": "Red","material_tracking"	: "Plastic",					"qc_tracking" : "Test",	"tag_warehouse_code" : "MFXXXXXX",					"partner_code" : "MFXXXXXX","weight_tracking" : 100,	"images":[{"tracking_code" : "SC5388176946","url" :"http://placehold.it/170x100&text=one"	},{"tracking_code" : "SC5388176946","url" :"http://placehold.it/170x100&text=one"}]}]} ]}'
```

```javascript
var data = JSON.stringify({
  "token": "xxxxxxxxx",
  "reference_code": "MF-TEST",
  "inventory_id": 186019,
  "entry_point": "VTPHN01",
  "overpacks": [
    {
      "description": "Test manifes boxme",
      "volume": "10x10x10",
      "weight": 1100,
      "color": "red",
      "total": 100,
      "overpack_partner_code": "MFXXXXXX",
      "manifest_orders": [
        {
          "tracking_code": "SC5388176946",
          "quantity_tracking": 50,
          "volume_tracking": "10x10x10",
          "color_tracking": "Red",
          "material_tracking": "Plastic",
          "qc_tracking": "Test",
          "tag_warehouse_code": "MFXXXXXX",
          "partner_code": "MFXXXXXX",
          "weight_tracking": 100,
          "images": [
            {
              "tracking_code": "SC5388176946",
              "url": "http://placehold.it/170x100&text=one"
            },
            {
              "tracking_code": "SC5388176946",
              "url": "http://placehold.it/170x100&text=one"
            }
          ]
        }
      ]
    }
  ]
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://services.boxme.vn/api/manifests");
xhr.setRequestHeader("content-type", "application/json");
xhr.setRequestHeader("cache-control", "no-cache");
xhr.setRequestHeader("postman-token", "d23635ff-98ac-0288-f985-ba65bc13fa35");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
    "manifest": {
        "overpacks": [
            {
                "overpack_reference_code": null,
                "overpack_weight": 1100,
                "_embedded": {
                    "data": [
                        {
                            "tracking_code": "SC5388176946",
                            "volume": "10x10x10",
                            "qc": "Test",
                            "weight": 100,
                            "overpack_code": "OPae0613",
                            "warehouse": "VTPHN01",
                            "images": [
                                {
                                    "tracking_code": "SC5388176946",
                                    "urls": "http://placehold.it/170x100&text=one"
                                },
                                {
                                    "tracking_code": "SC5388176946",
                                    "urls": "http://placehold.it/170x100&text=one"
                                }
                            ],
                            "material": "Plastic",
                            "color": "Red",
                            "quantity": 50
                        }
                    ]
                },
                "overpack_code": "OPae0613",
                "overpack_id": 244,
                "time_create": "2017-07-23 11:13:12.130950",
                "overpack_partner_code": "MFXXXXXX",
                "overpack_total": 100,
                "overpack_volume": "10x10x10"
            }
        ],
        "entry_point": "VTPHN01",
        "id": 147,
        "created": "2017-07-23 11:13:12.122047"
    },
    "error": "false"
}
```

### HTTP Request

`POST http://services.boxme.vn/api/manifests`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
token    | **string**    |yes | Your api key of seller.
reference_code | **string**    |yes | Partner reference code.
inventory_id | **string**    |yes | Id inventory of Boxme/Shipchung.
entry_point | **string**    |yes | Warehouse of Boxme.
overpacks.description | **string**    |yes | Description of Overpacks .
overpacks.volume | **string**    |yes | Volume of Overpacks .
overpacks.weight || **string**    |yes | Weight of Overpacks .
overpacks.color | **string**    |yes | Color of Overpacks .
overpacks.overpack_partner_code | **string**    |yes | Reference code of Overpacks .
overpacks.manifest_orders.tracking_code | **string**    |yes | Tracking code of Boxme/Shipchung.
overpacks.manifest_orders.quantity_tracking | **string**    |yes | Quantity of tracking code.
overpacks.manifest_orders.volume_tracking | **string**    |yes | Volume of tracking code.
overpacks.manifest_orders.color_tracking | **string**    |yes | Color of tracking code.
overpacks.manifest_orders.material_tracking | **string**    |yes | Material of tracking code.
overpacks.manifest_orders.qc_tracking | **string**    |yes | Quality control.
overpacks.manifest_orders.tag_warehouse_code | **string**    |yes | Warehouse partner reference code.
overpacks.manifest_orders.partner_code | **string**    |yes | Partner code.
overpacks.manifest_orders.weight_tracking | **string**    |yes | Weight of tracking code.
overpacks.manifest_orders.images.url | **string**    |yes | Partner reference code.


> The above command body JSON structured like this:

```json
{
    "token":"xxxxxxxxxxx",
    "reference_code":"MF-TEST",
    "inventory_id":144469,
    "entry_point":"VTPHN01",
    "overpacks":[
        {
            "description" : "Test manifes boxme",
            "volume" : "10x10x10",
            "weight" : 1100,
            "color" : "red",
            "total" : 100,
            "overpack_partner_code" : "MFXXXXXX",
            "manifest_orders":[
		        {
		            "tracking_code" 		: "SC123456789",
					"quantity_tracking" 	: 50,
					"volume_tracking" 		: "10x10x10",
					"color_tracking" 		: "Red",
					"material_tracking" 	: "Plastic",
					"qc_tracking" 			: "Test",
					"tag_warehouse_code" : "MFXXXXXX",
					"partner_code" : "MFXXXXXX",
					"weight_tracking" 		: 100,
					"images":[
						{
							"url" : "http://placehold.it/170x100&text=one"
						},
						{
							"url" : "http://placehold.it/170x100&text=one"
						}
					]
		        }
		    ]
        }
    ]
}
```

<aside class="notice">
You must with your personal API key.
</aside>


## Get list manifest
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://devwms.boxme.vn/api/list_manifests")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["content-type"] = 'application/json'
request.body = "{}"

response = http.request(request)
puts response.read_body

```

```python
import http.client

conn = http.client.HTTPConnection("devwms.boxme.vn")

payload = "{}"

headers = { 'content-type': "application/json" }

conn.request("GET", "/api/list_manifests", payload, headers)

res = conn.getresponse()
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://devwms.boxme.vn/api/list_manifests \
  --header 'content-type: application/json' \
  --data '{}'
```

```javascript
var data = JSON.stringify({});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://devwms.boxme.vn/api/list_manifests");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  total_items: 7,
  _embedded: {
    data: [
      {
        status: 1,
        created: "2017-07-13 16:59:07",
        manifest_code: "MF3daa18",
        inventory_id: 166911,
        entry_point: "VTPHCM01",
        reference_code: "WV-TEST"
      },
      {
        status: 1,
        created: "2017-07-13 17:03:32",
        manifest_code: "MF12a5d1",
        inventory_id: 166911,
        entry_point: "VTPHCM01",
        reference_code: "WV-TEST"
      },
      {
        status: 6,
        created: "2017-07-15 13:43:35",
        manifest_code: "MF761659",
        inventory_id: 183333,
        entry_point: "VTPHCM01",
        reference_code: "WV-218B"
      },
      {
        status: 3,
        created: "2017-07-18 11:53:07",
        manifest_code: "MF6a139a",
        inventory_id: 183333,
        entry_point: "VTPHCM01",
        reference_code: "WV-220"
      },
      {
        status: 3,
        created: "2017-07-18 16:02:14",
        manifest_code: "MF8e344d",
        inventory_id: 183333,
        entry_point: "VTPHCM01",
        reference_code: "WV-221"
      },
      {
        status: 3,
        created: "2017-07-20 15:21:26",
        manifest_code: "MFa297a4",
        inventory_id: 186019,
        entry_point: "VTPHCM01",
        reference_code: "WV-222"
      },
      {
        status: 2,
        created: "2017-07-21 16:02:00",
        manifest_code: "MF51df58",
        inventory_id: 186019,
        entry_point: "VTPHCM01",
        reference_code: "WV-223"
      }
    ]
  },
  page_count: 1,
  page_size: 25,
  _links: {
    next: {
      href: "/list_manifests?page=1"
    },
    self: {
      href: "/list_manifests"
    },
    prev: {
      href: "/list_manifests?page=1"
    },
    last: {
      href: "/list_manifests?page=1"
    },
    first: {
      href: "/list_manifests?page=1"
    }
  },
  error: "false"
}
```

### HTTP Request

`GET http://devwms.boxme.vn/api/list_manifests`

### Query Parameters


Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
token    | **string**    |yes | Your api key of seller.

<aside class="notice">
You must with your personal API key.
</aside>

## Get detail manifest
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://services.boxme.vn/api/manifests/manifest_code")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["content-type"] = 'application/json'
request.body = "{}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("http://wms.boxme.vn")

payload = "{}"

headers = { 'content-type': "application/json" }

conn.request("GET", "/api/manifests/manifest_code", payload, headers)

res = conn.getresponse()
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://services.boxme.vn/api/manifests/manifest_code \
  --header 'content-type: application/json' \
  --data '{}'
```

```javascript
var data = JSON.stringify({});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://services.boxme.vn/api/manifests/manifest_code");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  manifest: {
    status: 6,
    user_phone: "0902165440",
    manifest_code: "MF761659",
    inventory_id: 183333,
    total_ovepacked: 2,
    overpacks: [
      {
        total_order: 2,
        status: 2,
        weight: 2200,
        volume: "",
        total: 22,
        total_receive: 22,
        overpack_code: "OP248798",
        status_name: "Packed",
        overpack_partner_code: "WV-218B-BOX-4",
        created: "2017-07-15 13:43:36",
        overpack_id: "189"
      },
      {
        total_order: 1,
        status: 2,
        weight: 1500,
        volume: "",
        total: 9,
        total_receive: 9,
        overpack_code: "OPf747ab",
        status_name: "Packed",
        overpack_partner_code: "WV-218B-BOX-1",
        created: "2017-07-15 13:43:36",
        overpack_id: "190"
      }
    ],
    total: 31,
    status_name: "Closed",
    created: "2017-07-15 13:43:35",
    order_mf: [
      {
        status: 11,
        weight: 1900,
        tracking_code: "SC5836474452",
        overpack_id: 189,
        images: [
          {
            url: "http://admin.weshop.asia/uploads/warehouse_proxy/stock_in/2017/06/30/3691086/rdfus6ozxz6wu08.png"
          },
          {
            url: "http://admin.weshop.asia/uploads/warehouse_proxy/stock_in/2017/06/30/3691086/6klujinrzbap7ot.png"
          },
          {
            url: "http://admin.weshop.asia/uploads/warehouse_proxy/stock_in/2017/06/30/3691086/sqn6kl1bfpae1lb.png"
          }
        ],
        courier_id: 1,
        total: 12,
        check: "[7, 7]",
        volume: "NonexNonexNone",
        overpack_code: "OP248798",
        status_name: "Sẵn sàng giao hàng",
        created: "2017-07-15 13:43:36",
        warehouse: "VTPHCM01"
      },
      {
        status: 11,
        weight: 1700,
        tracking_code: "SC51537766825",
        overpack_id: 189,
        images: [
          {
            url: "http://admin.weshop.asia/uploads/warehouse_proxy/stock_in/2017/06/30/NT_1498776131016/dr1hmk58h1g8da7.png"
          },
          {
            url: "http://admin.weshop.asia/uploads/warehouse_proxy/stock_in/2017/06/30/NT_1498776131016/tmpfx0j7hmta1fd.png"
          }
        ],
        courier_id: 1,
        total: 10,
        check: "[7]",
        volume: "NonexNonexNone",
        overpack_code: "OP248798",
        status_name: "Sẵn sàng giao hàng",
        created: "2017-07-15 13:43:36",
        warehouse: "VTPHCM01"
      },
      {
        status: 11,
        weight: 1500,
        tracking_code: "SC51755077834",
        overpack_id: 190,
        images: [
          {
            url: "http://admin.weshop.asia/uploads/warehouse_proxy/stock_in/2017/06/30/NT_1498776877622/8z3yex5zha6qmtj.png"
          },
          {
            url: "http://admin.weshop.asia/uploads/warehouse_proxy/stock_in/2017/06/30/NT_1498776877622/58uycertaq28lbs.png"
          }
        ],
        courier_id: 1,
        total: 9,
        check: "[7, 7]",
        volume: "NonexNonexNone",
        overpack_code: "OPf747ab",
        status_name: "Sẵn sàng giao hàng",
        created: "2017-07-15 13:43:36",
        warehouse: "VTPHCM01"
      }
    ],
    entry_point: "VTPHCM01",
    user_name: "Weshop Vietnam"
  },
  error: "false"
}
```

### HTTP Request

`GET http://services.boxme.vn/api/manifests/manifest_code`

### Query Parameters


Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
token    | **string**    |yes | Your api key of seller.

<aside class="notice">
You must with your personal API key.
</aside>

## Update orders hold

> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://services.boxme.vn/api/update_orders_hold")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/x-www-form-urlencoded'
request["cache-control"] = 'no-cache'
request["postman-token"] = '2d8c1289-cc95-e4ae-acb8-358dd0b9ff2a'

response = http.request(request)
puts response.read_body
```

```python
import requests

url = "http://services.boxme.vn/api/update_orders_hold"

payload = ""
headers = {
    'content-type': "application/x-www-form-urlencoded",
    'cache-control': "no-cache",
    'postman-token': "5ecb3c24-4ffc-dfdb-b703-f083adf144fe"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
```

```shell
curl --request POST \
  --url http://services.boxme.vn/api/update_orders_hold \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --header 'postman-token: 49305424-7f22-727a-2fd0-2a0dc5d31680'
```

```javascript
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://services.boxme.vn/api/update_orders_hold");
xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
xhr.setRequestHeader("cache-control", "no-cache");
xhr.setRequestHeader("postman-token", "2ad0febf-f0e7-7475-49e9-7c5c0b956388");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
	'error' 	: 'false',
	'_embedded'	: {
	    'created'	: '2017-07-10 14:14:14',
	    'data'	: [
	    	{
	    	    'tracking_code' : 'SC5145506872',
	    	    'note'	: 'Cập nhật trạng thái đơn thành công.'	
	    }
	    ]
}
```

### HTTP Request

`POST http://services.boxme.vn/api/update_orders_hold`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
token    | **string**    |yes | Your api key of seller.
order_list.tracking_code        | **string**    |yes | Tracking code of seller BOXME/SHIPCUNG.
order_list.note        | **string**    |yes | Note.

> The above command body JSON structured like this:

```json
{
  "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  "order_list": [
    	{
    		"tracking_code" : "SC51627745352",
    		"note" : "Boxme test"
    	},
    	{
    		"tracking_code" : "SC5145506872",
    		"note" : "Boxme test"
    	}
    ]
}

```

<aside class="notice">
You must with your personal API key.
</aside>
