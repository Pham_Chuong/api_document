# Location
## CSV file 

You can download it here : **`https://bitbucket.org/loihv/boxme-location-database`**

## Get Province - City List
> To authorize, use this code:



```php
<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/api/public/api/merchant/rest/lading/city');
$request->setMethod(HTTP_METH_GET);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'content-type' => 'application/json'
));

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```


```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/api/merchant/rest/lading/city")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["headers"] = '{'content-type': 'application/x-www-form-urlencoded'}'
request["content-type"] = 'application/x-www-form-urlencoded'

response = http.request(request)
puts response.read_body
```

```python
import requests

url = "http://prod.boxme.vn/api/public/api/merchant/rest/lading/city"

payload = ""
headers = {
    'headers': "{'content-type': 'application/x-www-form-urlencoded'}",
    'content-type': "application/x-www-form-urlencoded"
    }

response = requests.request("GET", url, data=payload, headers=headers)

print(response.text)
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://prod.boxme.vn/api/public/api/merchant/rest/lading/city \
  --header 'content-type: application/x-www-form-urlencoded' \
  --header 'headers: {'\''content-type'\'': '\''application/x-www-form-urlencoded'\''}'
```

```javascript
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://prod.boxme.vn/api/public/api/merchant/rest/lading/city");
xhr.setRequestHeader("headers", "{'content-type': 'application/x-www-form-urlencoded'}");
xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "error": false,
  "error_message": "success",
  "data": [
    {
      "CityId": 18,
      "CityName": "Hà Nội"
    },
    {
      "CityId": 52,
      "CityName": "TP.Hồ Chí Minh"
    },
    {
      "CityId": 56,
      "CityName": "An Giang"
    },
    {
      "CityId": 54,
      "CityName": "Bà Rịa -  Vũng Tàu"
    },
    {
      "CityId": 19,
      "CityName": "Bắc Giang"
    },
    {
      "CityId": 6,
      "CityName": "Bắc Kạn"
    },
    {
      "CityId": 65,
      "CityName": "Bạc Liêu"
    },
    {
      "CityId": 2,
      "CityName": "Bắc Ninh"
    },
    {
      "CityId": 60,
      "CityName": "Bến Tre"
    },
    {
      "CityId": 49,
      "CityName": "Bình Dương"
    },
    {
      "CityId": 39,
      "CityName": "Bình Định "
    },
    {
      "CityId": 45,
      "CityName": "Bình Phước "
    },
    {
      "CityId": 51,
      "CityName": "Bình Thuận"
    },
    {
      "CityId": 68,
      "CityName": "Cà Mau"
    },
    {
      "CityId": 59,
      "CityName": "Cần Thơ"
    },
    {
      "CityId": 1,
      "CityName": "Cao Bằng"
    },
    {
      "CityId": 35,
      "CityName": "Đà Nẵng"
    },
    {
      "CityId": 42,
      "CityName": "Đăk Lăk "
    },
    {
      "CityId": 44,
      "CityName": "Đăk Nông"
    },
    {
      "CityId": 10,
      "CityName": "Điện Biên"
    },
    {
      "CityId": 50,
      "CityName": "Đồng Nai"
    },
    {
      "CityId": 57,
      "CityName": "Đồng Tháp"
    },
    {
      "CityId": 40,
      "CityName": "Gia Lai"
    },
    {
      "CityId": 3,
      "CityName": "Hà Giang"
    },
    {
      "CityId": 25,
      "CityName": "Hà Nam"
    },
    {
      "CityId": 31,
      "CityName": "Hà Tĩnh"
    },
    {
      "CityId": 22,
      "CityName": "Hải Dương"
    },
    {
      "CityId": 24,
      "CityName": "Hải Phòng"
    },
    {
      "CityId": 63,
      "CityName": "Hậu Giang"
    },
    {
      "CityId": 23,
      "CityName": "Hòa Bình"
    },
    {
      "CityId": 8,
      "CityName": "Hưng Yên"
    },
    {
      "CityId": 43,
      "CityName": "Khánh Hòa"
    },
    {
      "CityId": 62,
      "CityName": "Kiên Giang"
    },
    {
      "CityId": 38,
      "CityName": "Kon Tum"
    },
    {
      "CityId": 4,
      "CityName": "Lai Châu"
    },
    {
      "CityId": 46,
      "CityName": "Lâm Đồng"
    },
    {
      "CityId": 14,
      "CityName": "Lạng Sơn"
    },
    {
      "CityId": 5,
      "CityName": "Lào Cai"
    },
    {
      "CityId": 53,
      "CityName": "Long An "
    },
    {
      "CityId": 28,
      "CityName": "Nam Định"
    },
    {
      "CityId": 30,
      "CityName": "Nghệ An"
    },
    {
      "CityId": 27,
      "CityName": "Ninh Bình"
    },
    {
      "CityId": 47,
      "CityName": "Ninh Thuận"
    },
    {
      "CityId": 17,
      "CityName": "Phú Thọ"
    },
    {
      "CityId": 9,
      "CityName": "Phú Yên"
    },
    {
      "CityId": 32,
      "CityName": "Quảng Bình"
    },
    {
      "CityId": 36,
      "CityName": "Quảng Nam "
    },
    {
      "CityId": 37,
      "CityName": "Quảng Ngãi"
    },
    {
      "CityId": 20,
      "CityName": "Quảng Ninh"
    },
    {
      "CityId": 33,
      "CityName": "Quảng Trị"
    },
    {
      "CityId": 13,
      "CityName": "Sóc Trăng"
    },
    {
      "CityId": 16,
      "CityName": "Sơn La"
    },
    {
      "CityId": 48,
      "CityName": "Tây Ninh"
    },
    {
      "CityId": 26,
      "CityName": "Thái Bình"
    },
    {
      "CityId": 12,
      "CityName": "Thái Nguyên"
    },
    {
      "CityId": 29,
      "CityName": "Thanh Hóa"
    },
    {
      "CityId": 34,
      "CityName": "Thừa Thiên Huế"
    },
    {
      "CityId": 58,
      "CityName": "Tiền Giang"
    },
    {
      "CityId": 64,
      "CityName": "Trà Vinh"
    },
    {
      "CityId": 7,
      "CityName": "Tuyên Quang"
    },
    {
      "CityId": 61,
      "CityName": "Vĩnh Long"
    },
    {
      "CityId": 15,
      "CityName": "Vĩnh Phúc"
    },
    {
      "CityId": 11,
      "CityName": "Yên Bái"
    }
  ]
}
```

### HTTP Request

`GET http://prod.boxme.vn/api/public/api/merchant/rest/lading/city`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
headers    | **string**    |yes | 'content-type': 'application/x-www-form-urlencoded'.

<aside class="notice">
You must with your personal API key.
</aside>


## Get District(Province) List by City ID
> To authorize, use this code:



```php

<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/api/public/api/merchant/rest/lading/province/18');
$request->setMethod(HTTP_METH_GET);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'content-type' => 'application/json'
));

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/api/merchant/rest/lading/province/18")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["headers"] = '{'content-type': 'application/x-www-form-urlencoded'}'
request["content-type"] = 'application/x-www-form-urlencoded'

response = http.request(request)
puts response.read_body
```

```python
import requests

url = "http://prod.boxme.vn/api/public/api/merchant/rest/lading/province/18"

payload = ""
headers = {
    'headers': "{'content-type': 'application/x-www-form-urlencoded'}",
    'content-type': "application/x-www-form-urlencoded"
    }

response = requests.request("GET", url, data=payload, headers=headers)

print(response.text)
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://prod.boxme.vn/api/public/api/merchant/rest/lading/province/18 \
  --header 'content-type: application/x-www-form-urlencoded' \
  --header 'headers: {'\''content-type'\'': '\''application/x-www-form-urlencoded'\''}'
```

```javascript
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://prod.boxme.vn/api/public/api/merchant/rest/lading/province/18");
xhr.setRequestHeader("headers", "{'content-type': 'application/x-www-form-urlencoded'}");
xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "error": false,
  "error_message": "success",
  "data": [
    {
      "ProvinceId": 568,
      "ProvinceName": "Quận Thủ Đức",
      "Remote": 1
    },
    {
      "ProvinceId": 552,
      "ProvinceName": "Quận Tân Phú",
      "Remote": 1
    },
    {
      "ProvinceId": 551,
      "ProvinceName": "Quận Tân Bình",
      "Remote": 1
    },
    {
      "ProvinceId": 570,
      "ProvinceName": "Quận Phú Nhuận",
      "Remote": 1
    },
    {
      "ProvinceId": 561,
      "ProvinceName": "Quận Gò Vấp",
      "Remote": 1
    },
    {
      "ProvinceId": 569,
      "ProvinceName": "Quận Bình Thạnh",
      "Remote": 1
    },
    {
      "ProvinceId": 564,
      "ProvinceName": "Quận Bình Tân",
      "Remote": 1
    },
    {
      "ProvinceId": 558,
      "ProvinceName": "Quận 9",
      "Remote": 1
    },
    {
      "ProvinceId": 565,
      "ProvinceName": "Quận 8",
      "Remote": 1
    },
    {
      "ProvinceId": 556,
      "ProvinceName": "Quận 7",
      "Remote": 1
    },
    {
      "ProvinceId": 548,
      "ProvinceName": "Quận 6",
      "Remote": 1
    },
    {
      "ProvinceId": 549,
      "ProvinceName": "Quận 5",
      "Remote": 1
    },
    {
      "ProvinceId": 566,
      "ProvinceName": "Quận 4",
      "Remote": 1
    },
    {
      "ProvinceId": 550,
      "ProvinceName": "Quận 3",
      "Remote": 1
    },
    {
      "ProvinceId": 571,
      "ProvinceName": "Quận 2",
      "Remote": 1
    },
    {
      "ProvinceId": 557,
      "ProvinceName": "Quận 12",
      "Remote": 1
    },
    {
      "ProvinceId": 562,
      "ProvinceName": "Quận 11",
      "Remote": 1
    },
    {
      "ProvinceId": 555,
      "ProvinceName": "Quận 10",
      "Remote": 1
    },
    {
      "ProvinceId": 560,
      "ProvinceName": "Quận 1",
      "Remote": 1
    },
    {
      "ProvinceId": 554,
      "ProvinceName": "Huyện Nhà Bè",
      "Remote": 1
    },
    {
      "ProvinceId": 563,
      "ProvinceName": "Huyện Hóc Môn",
      "Remote": 1
    },
    {
      "ProvinceId": 553,
      "ProvinceName": "Huyện Củ Chi",
      "Remote": 1
    },
    {
      "ProvinceId": 559,
      "ProvinceName": "Huyện Cần Giờ",
      "Remote": 1
    },
    {
      "ProvinceId": 567,
      "ProvinceName": "Huyện Bình Chánh",
      "Remote": 1
    }
  ]
}
```

### HTTP Request

`GET http://prod.boxme.vn/api/public/api/merchant/rest/lading/province/18`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
headers    | **string**    |yes | 'content-type': 'application/x-www-form-urlencoded'.

<aside class="notice">
You must with your personal API key.
</aside>


## Get Ward by District ID
> To authorize, use this code:

```php
<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/api/public/api/merchant/rest/lading/ward/172');
$request->setMethod(HTTP_METH_GET);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'content-type' => 'application/json'
));

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/api/merchant/rest/lading/ward/172")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["headers"] = '{'content-type': 'application/x-www-form-urlencoded'}'
request["content-type"] = 'application/x-www-form-urlencoded'

response = http.request(request)
puts response.read_body
```

```python
import requests

url = "http://prod.boxme.vn/api/public/api/merchant/rest/lading/ward/172"

payload = ""
headers = {
    'headers': "{'content-type': 'application/x-www-form-urlencoded'}",
    'content-type': "application/x-www-form-urlencoded"
    }

response = requests.request("GET", url, data=payload, headers=headers)

print(response.text)
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://prod.boxme.vn/api/public/api/merchant/rest/lading/ward/172 \
  --header 'content-type: application/x-www-form-urlencoded' \
  --header 'headers: {'\''content-type'\'': '\''application/x-www-form-urlencoded'\''}'
```

```javascript
var data = null;

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://prod.boxme.vn/api/public/api/merchant/rest/lading/ward/172");
xhr.setRequestHeader("headers", "{'content-type': 'application/x-www-form-urlencoded'}");
xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "error": false,
  "error_message": "success",
  "data": [
    {
      "WardId": 6707,
      "WardName": "Thị Trấn đoan Hùng"
    },
    {
      "WardId": 6726,
      "WardName": "Xã Bằng Doãn"
    },
    {
      "WardId": 6710,
      "WardName": "Xã Bằng Luân"
    },
    {
      "WardId": 6711,
      "WardName": "Xã Ca đình"
    },
    {
      "WardId": 6732,
      "WardName": "Xã Chân Mộng"
    },
    {
      "WardId": 6713,
      "WardName": "Xã Chi đám"
    },
    {
      "WardId": 6720,
      "WardName": "Xã đại Nghĩa"
    },
    {
      "WardId": 6723,
      "WardName": "Xã đông Khê"
    },
    {
      "WardId": 6712,
      "WardName": "Xã Hùng Long"
    },
    {
      "WardId": 6725,
      "WardName": "Xã Hùng Quan"
    },
    {
      "WardId": 6729,
      "WardName": "Xã Hữu đô"
    },
    {
      "WardId": 6731,
      "WardName": "Xã Minh Phú"
    },
    {
      "WardId": 6724,
      "WardName": "Xã Nghinh Xuyên"
    },
    {
      "WardId": 6708,
      "WardName": "Xã Ngọc Quan"
    },
    {
      "WardId": 6727,
      "WardName": "Xã Phong Phú"
    },
    {
      "WardId": 6721,
      "WardName": "Xã Phú Thứ"
    },
    {
      "WardId": 6728,
      "WardName": "Xã Phúc Lai"
    },
    {
      "WardId": 6715,
      "WardName": "Xã Phương Trung"
    },
    {
      "WardId": 6722,
      "WardName": "Xã Quế Lâm"
    },
    {
      "WardId": 6714,
      "WardName": "Xã Sóc đăng"
    },
    {
      "WardId": 6709,
      "WardName": "Xã Tây Cốc"
    },
    {
      "WardId": 6717,
      "WardName": "Xã Tiêu Sơn"
    },
    {
      "WardId": 6719,
      "WardName": "Xã Vân Du"
    },
    {
      "WardId": 6718,
      "WardName": "Xã Vân đồn"
    },
    {
      "WardId": 6730,
      "WardName": "Xã Vụ Quang"
    },
    {
      "WardId": 6716,
      "WardName": "Xã Yên Kiện"
    }
  ]
}
```

### HTTP Request

`GET http://prod.boxme.vn/api/public/api/merchant/rest/lading/ward/172`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
headers    | **string**    |yes | 'content-type': 'application/x-www-form-urlencoded'.

<aside class="notice">
You must with your personal API key.
</aside>