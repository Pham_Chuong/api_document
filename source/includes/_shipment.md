# Shipment
## Create Shipment
> To authorize, use this code:

```php
<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/api/public/bxapi/shipment-sdk');
$request->setMethod(HTTP_METH_POST);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'content-type' => 'application/json'
));

$request->setBody('{"ApiKey":"YOUR API_KEY","ShipToAddress":{"InventoryId":133455},"ShipFromAddress":{"InventoryId":105341},"TypeTransport":1,"ShipmentStatus":"ReadyToShip","Volumes":"10x10x10","Weight":500,"ShippingMethod":"MYSELF","ShipmentItems":[{"SKU":"14398602","QuantityShipped":100}]}');

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```


```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/bxapi/shipment-sdk")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"ShipToAddress\":{\"InventoryId\":133455},\"ShipFromAddress\":{\"InventoryId\":105341},\"TypeTransport\":1,\"ShipmentStatus\":\"ReadyToShip\",\"Volumes\":\"10x10x10\",\"Weight\":500,\"ShippingMethod\":\"MYSELF\",\"ShipmentItems\":[{\"SKU\":\"14398602\",\"QuantityShipped\":100}]}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"ShipToAddress\":{\"InventoryId\":133455},\"ShipFromAddress\":{\"InventoryId\":105341},\"TypeTransport\":1,\"ShipmentStatus\":\"ReadyToShip\",\"Volumes\":\"10x10x10\",\"Weight\":500,\"ShippingMethod\":\"MYSELF\",\"ShipmentItems\":[{\"SKU\":\"14398602\",\"QuantityShipped\":100}]}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/api/public/bxapi/shipment-sdk", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/bxapi/shipment-sdk \
  --header 'content-type: application/json' \
  --data '{"ApiKey":"7e4e80c6bdb03c5a2d639c4828bcf156","ShipToAddress":{"InventoryId":133455},"ShipFromAddress":{"InventoryId":105341},"TypeTransport":1,"ShipmentStatus":"ReadyToShip","Volumes":"10x10x10","Weight":500,"ShippingMethod":"MYSELF","ShipmentItems":[{"SKU":"14398602","QuantityShipped":100}]}'
```

```javascript
var data = JSON.stringify({
  "ApiKey": "7e4e80c6bdb03c5a2d639c4828bcf156",
  "ShipToAddress": {
    "InventoryId": 133455
  },
  "ShipFromAddress": {
    "InventoryId": 105341
  },
  "TypeTransport": 1,
  "ShipmentStatus": "ReadyToShip",
  "Volumes": "10x10x10",
  "Weight": 500,
  "ShippingMethod": "MYSELF",
  "ShipmentItems": [
    {
      "SKU": "14398602",
      "QuantityShipped": 100
    }
  ]
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/bxapi/shipment-sdk");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "inventory": [
    {
      "City": "TP. Hồ Chí Minh",
      "Code": null,
      "Name": "Kho - Huyện Nhà Bè - TP.Hồ Chí Minh",
      "District": "Huyện Nhà Bè",
      "Phone": "0938131593",
      "InventoryId": "92932",
      "Type": 0,
      "AddressLine": "aaa"
    }
  ]
}
```

### HTTP Request

`POST http://prod.boxme.vn/api/public/bxapi/shipment-sdk`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
ApiKey (MerchantKey)    | **string**    |yes | Your api key of seller.

<aside class="notice">
You must with your personal API key.
</aside>



## Accept Shipment
> To authorize, use this code:

```php
<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/bxapi/approve_shipment_by_partner');
$request->setMethod(HTTP_METH_POST);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'content-type' => 'application/json'
));

$request->setBody('{
"ApiKey":"xxxxxxxxxxxxxxxx",
"Shipment_Code":"xxxx-xxxx"}');

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```


```ruby
rrequire 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/bxapi/approve_shipment_by_partner")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request["cache-control"] = 'no-cache'
request.body = "{\n\"ApiKey\":\"xxxxxxxxxxxxx\",\n\"Shipment_Code\":\"xxxx-xxxx"}"

response = http.request(request)
puts response.read_body
```

```python
import requests

url = "http://prod.boxme.vn/bxapi/approve_shipment_by_partner"

payload = "{\n\"ApiKey\":\"xxxxxxxxxxxxx\",\n\"Shipment_Code\":\"xxxx-xxxx"}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
```

```shell
curl --request POST \
  --url http://prod.boxme.vn/bxapi/approve_shipment_by_partner \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/json' \
  --header 'postman-token: 70c299ca-1136-5f80-e127-781b8a06349c' \
  --data '{\n"ApiKey":"xxxxxxxxxxxxx",\n"Shipment_Code":"xxxx-xxxx"}'
```

```javascript
var data = JSON.stringify({
  "ApiKey": "xxxxxxxxxxxxx",
  "Shipment_Code": "KA-07"
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/bxapi/approve_shipment_by_partner");
xhr.setRequestHeader("content-type", "application/json");
xhr.setRequestHeader("cache-control", "no-cache")

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
      'shipment_code' : 'BX111111111',
      'message'       : 'Cập nhật trạng thái thành công!',
      'error'         : 'false'
}
```

### HTTP Request

`POST http://prod.boxme.vn/bxapi/approve_shipment_by_partner`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
ApiKey (MerchantKey)    | **string**    |yes | Your api key of seller.
Shipment_Code   | **string**    |yes | Your shipment code.

<aside class="notice">
You must with your personal API key.
</aside>