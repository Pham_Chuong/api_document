# International Shipping

Steps to create an international order:

 * Step 1: Get country
 * Step 2: GET city from country
 * Step 3: Calculate fee
 * Step 4: Create order


## Get country

> The above command returns JSON structured like this:

```json
    {
    "error": false,
    "data": [{
              "id": 1,
              "country_code": "AF",
              "country_name": "Afghanistan",
              "active": 1,
              "required_zipcode": 0,
              "phone_code": 93
            },{
              "id": 2,
              "country_code": "AL",
              "country_name": "Albania",
              "active": 1,
              "required_zipcode": 0,
              "phone_code": 355
        }]
 }      
```

### HTTP Request
`GET https://seller.shipchung.vn/api/public/api/base/country`
<aside class="notice">
You must with your personal API key.
</aside>


## Get city from country

> The above command returns JSON structured like this:

```json
{
    "error": false,
    "data": [{
            "id": 742492,
            "_id": 742492,
            "country_id": 230,
            "country_code": "US",
            "country_name": "UNITED STATES OF AMERICA",
            "state_code": "MN",
            "state_name": "MINNESOTA",
            "city_name": "NEW YORK MILLS",
            "city_name_local": null,
            "airport": "STP",
            "zipcode": "56567",
            "to_zipcode": "\u000056567",
            "col9": "STP",
            "col10": "",
            "region": null,
            "priority": null,
            "code": null
        }]
 }        

```
### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
id_country    | false   | ID country get from "api get country".
keyword       | false   | Name city in  country.


### HTTP Request
`GET https://seller.shipchung.vn/api/public/api/v1/mobile/city-global/{id_country}?q={keyword}`
<aside class="notice">
You must with your personal API key.
</aside>


## Calculate Fee

### HTTP Request
`POST http://services.shipchung.vn/api/rest/global/calculate`


### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
|ApiKey (MerchantKey)|  Required  | Your api key of seller.|
|From.City            |  Required  | From city send order.     |
|From.Province        |  Required  | From province send order.|
|From.Stock           |  Required  | ID inventory |
|To.Country           |  Required  | Id country |
|To.City              |  Required  | Id city |
|Order.Weight         |  Required  | Order weight.|
|Order.Amount         |  Required  | Total product value|
|Order.Quantity       |  Required  | Quantity.|
|Order.ProductName    |  Required  | Name product |
|Config.Service        |  Required | Shipping method:9. Economy delivery service,8. Express delivery| 
|Type      |  Required |   “Type”: “excel” : We auto select best courier. You don’t define params Courier.|
--------- | ------- | -----------

> The body request JSON structured like this:


```json 
{"Domain": "seller.shipchung.vn",
  "From": {
    "City": 52,
    "Province": 551,
    "Stock": 206963,
    "Address": "18h cong hoa",
    "Phone": "0123456789",
    "Name": "Dev"
  },
  "To": {
   "Country": 230,
    "City": 750012,
    "Address": "a",
    "Zipcode": "11444"
  },
  "Order": {
    "Weight": 100,
    "Amount": 200000,
    "Quantity": 1,
    "ProductName": "Book"
  },
  "Config": {
    "Service": 8,
    "Checking": 1,
    "Fragile": 2,
    "AutoAccept": 0
  },
  "MerchantKey": ""
}
```

> The above command returns JSON structured like this:


```json
{
    "error": false,
    "code": "SUCCESS",
    "message": "success",
    "error_message": "success",
    "data": {
        "courier": {
            "system": [
                {
                    "courier_id": 17,
                    "courier_name": "DHL",
                    "courier_description": "",
                    "courier_logo": "https://seller.shipchung.vn/img/logo-hvc/17.png",
                    "money_pickup": 0,
                    "leatime_delivery": 120,
                    "leatime_courier": 1,
                    "leatime_total": 120,
                    "leatime_ward": 0,
                    "leatime_str": "5 ngày",
                    "priority": 9,
                    "fee": {
                        "fee_id": 1364,
                        "detail_id": 59647,
                        "pvc": 856548,
                        "collect": 0,
                        "vas": {
                            "cod": 0,
                            "protected": 0
                        },
                        "discount": {
                            "pvc": 0,
                            "pcod": 0
                        }
                    }
                },
                {
                    "courier_id": 15,
                    "courier_name": "UPS",
                    "courier_description": "",
                    "courier_logo": "https://seller.shipchung.vn/img/logo-hvc/15.png",
                    "money_pickup": 0,
                    "leatime_delivery": 72,
                    "leatime_courier": 1,
                    "leatime_total": 72,
                    "leatime_ward": 0,
                    "leatime_str": "3 ngày",
                    "priority": 9,
                    "fee": {
                        "fee_id": 2109,
                        "detail_id": 61480,
                        "pvc": 720196,
                        "collect": 0,
                        "vas": {
                            "cod": 0,
                            "protected": 0
                        },
                        "discount": {
                            "pvc": 0,
                            "pcod": 0
                        }
                    }
                }
            ]
        }
    },
    "stock": 206963
}     

```
<aside class="notice">
You must with your personal API key.
</aside>


## Create Order

### HTTP Request
`POST http://services.shipchung.vn/api/rest/global/calculate`


### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
|ApiKey (MerchantKey)   |  Required| Your api key of seller.|
|From.City              |  Required| From city send order.     |
|From.Province          |  Required| From province send order.|
|From.Stock             |  Required|  ID inventory |
|To.Country             |  Required| Id country |
|To.City                |  Required| Id city |
|To.Zipcode             |  Required| Zipcode to city |
|To.Address             |  Required| addresss |
|To.Name                |  Required| Name buyer |
|To.Phone               |  Required| Phone number |
|To.PhoneCode           |  Required| Phone number |
|Order.Weight           |  Required| Order weight.|
|Order.Amount           |  Required| Total product value|
|Order.Quantity         |  Required| Quantity.|
|Order.ProductName      |  Required| Name product |
|Config.Service         |  Required| Shipping method:9. Economy delivery service,8. Express delivery| 
|Config.Checking        |  Required|Accept receiver view item in order before confirm received or not.|
|Config.AutoAccept      |  Required|   1.Auto accept 0. No accept (default).|
|Config.Fragile         |  Required|   Fragile.|
|Type                   |  Required|   “Type”: “excel” : We auto select best courier. You don’t define params Courier.|


> The body request JSON structured like this:


```json
{
  "Domain": "seller.shipchung.vn",
  "From": {
    "City": 52,
    "Province": 551,
    "Stock": 206963,
    "Ward": 4394,
    "Address": "18h cong hoa",
    "Phone": "0123456789",
    "Name": "dev"
  },
  "To": {
   "Country": 230,
    "City": 750012,
    "Address": "a",
    "Zipcode": "11444",
    "Phone": "123456789",
    "PhoneCode": 1,
    "Name": "Jack"
  },
  "Order": {
    "Weight": 100,
    "Amount": 200000,
    "Quantity": 1,
    "ProductName": "Book"
  },
  "Config": {
    "Service": 8,
    "Checking": 1,
    "Fragile": 2,
    "AutoAccept": 0
  },
  "Type":"excel",
  "MerchantKey": ""
}
```

> The above command returns JSON structured like this:


```json
{
    "error": false,
    "code": "SUCCESS",
    "message": "success",
    "data": {
        "TrackingCode": "SC51851803887",
        "CourierId": 17,
        "ShowFee": {
            "pvc": 856548
        }
    }
}  

```
<aside class="notice">
You must with your personal API key.
</aside>