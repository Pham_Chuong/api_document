# Product
## Get Lists Inventory
> To authorize, use this code:

```php
<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/bxapi/list_inventory-sdk');
$request->setMethod(HTTP_METH_GET);

$request->setQueryData(array(
  'ApiKey' => '7c2ab950ddfe7973d792fbaeafc288fb'
));

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'content-type' => 'application/x-www-form-urlencoded'
));

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/bxapi/list_inventory-sdk?ApiKey=7c2ab950ddfe7973d792fbaeafc288fb")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["content-type"] = 'application/x-www-form-urlencoded'
request["cache-control"] = 'no-cache'

response = http.request(request)
puts response.read_body
```

```python
import requests

url = "http://prod.boxme.vn/bxapi/list_inventory-sdk"

querystring = {"ApiKey":"7c2ab950ddfe7973d792fbaeafc288fb"}

headers = {
    'content-type': "application/x-www-form-urlencoded",
    'cache-control': "no-cache"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
```

```shell
curl --request GET \
  --url 'http://prod.boxme.vn/bxapi/list_inventory-sdk?ApiKey=7c2ab950ddfe7973d792fbaeafc288fb' \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/x-www-form-urlencoded'


```
> The above command returns JSON structured like this:

```json
{
  "inventory": [
    {
      "City": "TP. Hồ Chí Minh",
      "Code": null,
      "Name": "Kho - Huyện Nhà Bè - TP.Hồ Chí Minh",
      "District": "Huyện Nhà Bè",
      "Phone": "0938131593",
      "InventoryId": "92932",
      "Type": 0,
      "AddressLine": "aaa"
    }
  ]
}
```

### HTTP Request

`GET http://prod.boxme.vn/bxapi/list_inventory-sdk`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
ApiKey (MerchantKey)    | **string**    |yes | Your api key of seller.

<aside class="notice">
You must with your personal API key.
</aside>

## Edit Product

> To authorize, use this code:

```php
<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/bxapi/edit-product-sdk');
$request->setMethod(HTTP_METH_POST);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'content-type' => 'application/json'
));

$request->setBody('{"InventoryId":"137123","ApiKey":"YOUR API_KEY","SellerSKU":"KA-07","Name":"iphone 9 ","Description":"Iphone 7s black new 100%","BasePrice":10000,"SalePrice":20000,"Weight":1223,"Volume":"","ProductImages":""}');

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/bxapi/edit-product-sdk")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"InventoryId\":\"137123\",\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"SellerSKU\":\"KA-07\",\"Name\":\"iphone 9 \",\"Description\":\"Iphone 7s black new 100%\",\"BasePrice\":10000,\"SalePrice\":20000,\"Weight\":1223,\"Volume\":\"\",\"ProductImages\":\"\"}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{\"InventoryId\":\"137123\",\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"SellerSKU\":\"KA-07\",\"Name\":\"iphone 9 \",\"Description\":\"Iphone 7s black new 100%\",\"BasePrice\":10000,\"SalePrice\":20000,\"Weight\":1223,\"Volume\":\"\",\"ProductImages\":\"\"}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/bxapi/edit-product-sdk", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/bxapi/edit-product-sdk \
  --header 'content-type: application/json' \
  --data '{"InventoryId":"137123","ApiKey":"7e4e80c6bdb03c5a2d639c4828bcf156","SellerSKU":"KA-07","Name":"iphone 9 ","Description":"Iphone 7s black new 100%","BasePrice":10000,"SalePrice":20000,"Weight":1223,"Volume":"","ProductImages":""}'
```

```javascript
var data = JSON.stringify({
  "InventoryId": "137123",
  "ApiKey": "7e4e80c6bdb03c5a2d639c4828bcf156",
  "SellerSKU": "KA-07",
  "Name": "iphone 9 ",
  "Description": "Iphone 7s black new 100%",
  "BasePrice": 10000,
  "SalePrice": 20000,
  "Weight": 1223,
  "Volume": "",
  "ProductImages": ""
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/bxapi/edit-product-sdk");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "Name": "iphone 9 ",
  "SalePrice": 20000,
  "BasePrice": 10000,
  "SellerId": "87695",
  "Description": "Iphone 7s black new 100%",
  "QuantityUnit": null,
  "SellerSKU": "KA-07",
  "Weight": 1223,
  "Volume": null,
  "InventoryId": "137123",
  "ProductId": "88999"
}
```

### HTTP Request

`POST http://prod.boxme.vn/bxapi/edit-product-sdk`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
ApiKey (MerchantKey)    | **string**    |yes | Your api key of seller.
SellerSKU     | **string**    |yes | Product code (sku) to edit.
Name          | **string**    |yes | New product name.
InventoryId   | **string**    |yes | New ID warehouse (Pickup address config).
Description   | **string**    |yes | New product description .
BasePrice     | **string**    |yes | New product base price  .
SalePrice     | **string**    |yes | New product sale price (Price to create orders)  .
Weight        | **string**    |yes | New product weight (weight to create orders)  .
Volume        | **string**    |yes |  New Volume product  .

<aside class="notice">
You must with your personal API key.
</aside>

## Add Product

> To authorize, use this code:

```php
<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/bxapi/product-sdk');
$request->setMethod(HTTP_METH_POST);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'content-type' => 'application/json'
));

$request->setBody('{"InventoryId":"105341","ApiKey":"YOUR API_KEY","SellerSKU":"Test-Pro-IP7S","Name":"IPhone 7s Black","CategoryName":"Phone","SupplierName":"Apple","BrandName":"Apple","Description":"Iphone 7s black new 100%","ProductTags":"iphone,iphone7s,iphone7","Quantity":50,"BasePrice":16000000,"SalePrice":16500000,"BarcodeManufacturer":"IP7SABC","ModelName":"IPhone7s","Weight":200,"Volume":"2x2x2","ProductImages":""}');

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/bxapi/product-sdk")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"InventoryId\":\"105341\",\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"SellerSKU\":\"Test-Pro-IP7S\",\"Name\":\"IPhone 7s Black\",\"CategoryName\":\"Phone\",\"SupplierName\":\"Apple\",\"BrandName\":\"Apple\",\"Description\":\"Iphone 7s black new 100%\",\"ProductTags\":\"iphone,iphone7s,iphone7\",\"Quantity\":50,\"BasePrice\":16000000,\"SalePrice\":16500000,\"BarcodeManufacturer\":\"IP7SABC\",\"ModelName\":\"IPhone7s\",\"Weight\":200,\"Volume\":\"2x2x2\",\"ProductImages\":\"\"}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{\"InventoryId\":\"105341\",\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"SellerSKU\":\"Test-Pro-IP7S\",\"Name\":\"IPhone 7s Black\",\"CategoryName\":\"Phone\",\"SupplierName\":\"Apple\",\"BrandName\":\"Apple\",\"Description\":\"Iphone 7s black new 100%\",\"ProductTags\":\"iphone,iphone7s,iphone7\",\"Quantity\":50,\"BasePrice\":16000000,\"SalePrice\":16500000,\"BarcodeManufacturer\":\"IP7SABC\",\"ModelName\":\"IPhone7s\",\"Weight\":200,\"Volume\":\"2x2x2\",\"ProductImages\":\"\"}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/bxapi/product-sdk", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/bxapi/product-sdk \
  --header 'content-type: application/json' \
  --data '{"InventoryId":"105341","ApiKey":"7e4e80c6bdb03c5a2d639c4828bcf156","SellerSKU":"Test-Pro-IP7S","Name":"IPhone 7s Black","CategoryName":"Phone","SupplierName":"Apple","BrandName":"Apple","Description":"Iphone 7s black new 100%","ProductTags":"iphone,iphone7s,iphone7","Quantity":50,"BasePrice":16000000,"SalePrice":16500000,"BarcodeManufacturer":"IP7SABC","ModelName":"IPhone7s","Weight":200,"Volume":"2x2x2","ProductImages":""}'
```

```javascript
var data = JSON.stringify({
  "InventoryId": "105341",
  "ApiKey": "7e4e80c6bdb03c5a2d639c4828bcf156",
  "SellerSKU": "Test-Pro-IP7S",
  "Name": "IPhone 7s Black",
  "CategoryName": "Phone",
  "SupplierName": "Apple",
  "BrandName": "Apple",
  "Description": "Iphone 7s black new 100%",
  "ProductTags": "iphone,iphone7s,iphone7",
  "Quantity": 50,
  "BasePrice": 16000000,
  "SalePrice": 16500000,
  "BarcodeManufacturer": "IP7SABC",
  "ModelName": "IPhone7s",
  "Weight": 200,
  "Volume": "2x2x2",
  "ProductImages": ""
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/bxapi/product-sdk");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "WeightUnit": 1,
  "BrandName": "brank",
  "SalePrice": 2000,
  "BasePrice": 1000,
  "SupplierName": "soupper",
  "Name": "San pham Test",
  "Description": "Description testt",
  "Volume": "2x2x2",
  "ManufactureBarcode": null,
  "SellerId": "30788",
  "Quantity": 200,
  "ModelName": "moel",
  "SupplierId": null,
  "ModelId": null,
  "ExternalUrl": null,
  "CategoryId": null,
  "CategoryName": "Dien Thoai",
  "QuantityUnit": 1,
  "SellerSKU": "Biaaaa",
  "Weight": 200,
  "BrandId": "",
  "InventoryId": "36",
  "ProductTags": "tag 1 tag 2",
  "ProductId": "11584"
}
```

### HTTP Request

`POST http://prod.boxme.vn/bxapi/product-sdk`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
ApiKey (MerchantKey)    | **string**    |yes | Your api key of seller.
SellerSKU     | **string**    |yes | Product code (sku) to edit.
InventoryId   | **string**    |yes | Id inventory (not boxme).
Name          | **string**    |yes | Product Name.
CategoryName  | **string**    |yes | Product CategoryName .
SupplierName  | **string**    |yes | Product SupplierName  .
BrandName     | **string**    |yes | Product Brand.
Description   | **string**    |yes | Product Description.
ProductTags   | **string**    |yes | Product tag.
Quantity      | **string**    |yes | Product quantity.
BasePrice     | **string**    |yes | New product base price.
SalePrice     | **string**    |yes | New product sale price (Price to create orders) .
Weight        | **string**    |yes | New product weight (weight to create orders).
BarcodeManufacturer     | **string**    |yes | Barcode (if exist).
ProductImages | **string**    |yes | Images (if exist) .

<aside class="notice">
You must with your personal API key.
</aside>


## Get Lists Product

> To authorize, use this code:


```php
<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/bxapi/list_product-sdk');
$request->setMethod(HTTP_METH_GET);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'apikey' => 'Your API_KEY',
  'content-type' => 'application/json'
));

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```


```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/bxapi/list_product-sdk")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["content-type"] = 'application/json'
request.body = "{}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{}"

headers = { 'content-type': "application/json" }

conn.request("GET", "/bxapi/list_product-sdk", payload, headers)

res = conn.getresponse()
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://prod.boxme.vn/bxapi/list_product-sdk \
  --header 'content-type: application/json' \
  --data '{}'
```

```javascript
var data = JSON.stringify({});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://prod.boxme.vn/bxapi/list_product-sdk");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "total_items": 26,
  "data": {
    "product": [
      {
        "Name": "iphone 9 ",
        "InventoryName": "Giau test",
        "SalePrice": 200000,
        "BasePrice": 10000,
        "Description": "Iphone 7s black new 100%",
        "SellerSKU": "A-377AAEE",
        "Weight": 1223,
        "ProductImages": [],
        "InventoryId": 137123,
        "Quantity": 1,
        "ProductId": "89034",
        "ToBoxme": false
      }
    ]
  }
}
```

### HTTP Request

`GET http://prod.boxme.vn/bxapi/list_product-sdk`

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
ApiKey (MerchantKey)    | **string**    |yes | Your api key of seller.

<aside class="notice">
You must with your personal API key.
</aside>

## Get List Product To Create Order

> To authorize, use this code:


```php
<?php

$request = new HttpRequest();
$request->setUrl('http://prod.boxme.vn/bxapi/get_list_products_create_order');
$request->setMethod(HTTP_METH_GET);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'apikey' => 'Your API_KEY',
  'content-type' => 'application/json'
));

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}
```

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/bxapi/get_list_products_create_order")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["content-type"] = 'application/json'
request.body = "{}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{}"

headers = { 'content-type': "application/json" }

conn.request("GET", "/bxapi/get_list_products_create_order", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://prod.boxme.vn/bxapi/get_list_products_create_order \
  --header 'content-type: application/json' \
  --data '{}'
```

```javascript
var data = JSON.stringify({});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://prod.boxme.vn/bxapi/get_list_products_create_order");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "total_items": 1,
  "data": {
    "product": [
      {
        "InventoryName": "Vĩnh Lộc - HCM",
        "Waiting": 0,
        "InventoryLimit": 0,
        "SalePrice": "150000",
        "SellerSKU": "PO-150M4NU",
        "Weight": 200,
        "InventoryId": 133902,
        "Name": "Đồng hồ CẶP thời trang Pollock + 2 Pin dự phòng",
        "Quantity": 24,
        "ProductId": "88954"
      }
    ]
  }
}
```

### HTTP Request


<strong><span style="color: rgb(51,102,255);">GET http://prod.boxme.vn/bxapi/get_list_products_create_order</span></strong>

### Query Parameters

Parameter |   Type      |    Required      |Description
--------- | ----------- | ----------- |-----------
ApiKey (MerchantKey)    | **string**    |yes | Your api key of seller.
inventory_id  | **string**    |yes | ID inventory(boxme).
sellerSKU     | **string**    |yes | BSIN(SKU).

<aside class="notice">
You must with your personal API key.
</aside>